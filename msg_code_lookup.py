from app import app, db
from app.models import Application, MsgCode


def add_dummy_data():
    m1 = MsgCode(
        code="ABC2324", description="Oh no!", resolution="Yeah, well...")
    m2 = MsgCode(
        code="ABCsad23",
        description="ljasdap pokads aposd cds",
        resolution="asdsa sdcds sc")
    m3 = MsgCode(
        code="132sdcdsc",
        description="asd sc",
        resolution="csasdcsd dscdsc sdcsadc")
    a = Application(name="Yeah!BAEM", msgcodes=[m1, m2, m3])
    db.session.add(a)
    db.session.commit()


@app.shell_context_processor
def make_shell_context():
    return {
        'db': db,
        'Application': Application,
        'MsgCode': MsgCode,
        'add_dummy_data': add_dummy_data
    }
