from app import app
from app import logger
from flask import Flask, jsonify, request, render_template, Markup, redirect, url_for
from CommonMark import commonmark as md
from app.models import Application, MsgCode


# Definition of the routes. Put them into their own file. See also
# Flask Blueprints: http://flask.pocoo.org/docs/latest/blueprints
@app.route("/")
def index():
    logger.info("/")
    with open("README.md") as f:
        readme = Markup(md(f.read()))
    if request.args.get('c'):
        msgcode = MsgCode.query.filter_by(
            code=request.args.get('c')).first_or_404()
        return redirect(
            url_for(
                'errorCode',
                appName=msgcode.application.name,
                code=msgcode.code))
    applications = Application.query.all()
    return render_template(
        'index.html', title='Home', readme=readme, applications=applications)


@app.route("/foo/<someId>")
def foo_url_arg(someId):
    logger.info("/foo/%s", someId)
    return jsonify({"echo": someId})


@app.route("/<appName>/")
def appInfo(appName):
    logger.info("/%s", appName)
    appl = Application.query.filter_by(name=appName).first_or_404()
    return render_template("app.html", title=appl.name, app=appl)
    #return jsonify({"request.args": request.args, "request.path": request.path})


@app.route("/<appName>/<code>")
def errorCode(appName, code):
    logger.info("/%s/%s", appName, code)
    app = Application(name=appName)
    msgcode = MsgCode.query.filter_by(code=code).first_or_404()
    return render_template("code.html", title=code, code=msgcode, app=app)
    #return jsonify({"request.args": request.args, "request.path": request.path})
