"""
Documentation

See also https://www.python-boilerplate.com/flask
"""
import os

import retry

from flask import Flask, jsonify, request, render_template, Markup
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_alembic import Alembic

from logzero import logger

# local imports
from config import Config


def create_app(config=None):
    app = Flask(__name__)

    app.config.from_object(config)

    # Setup cors headers to allow all domains
    # https://flask-cors.readthedocs.io/en/latest/
    CORS(app)

    return app


port = int(os.environ.get("PORT", 5000))

app = create_app(config=Config)
db = SQLAlchemy(app)
alembic = Alembic(app)

from sqlalchemy.exc import OperationalError


@retry.retry(
    exceptions=(OperationalError),
    delay=5,
    backoff=1.1,
    jitter=1,
    logger=app.logger)
def do_upgrade():
    alembic.upgrade()


with app.app_context():
    do_upgrade()

#local imports which depend on 'app'
from app import routes, models

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=port)
