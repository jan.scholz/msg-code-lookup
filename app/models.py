from app import db


class Application(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=True)
    msgcodes = db.relationship(
        'MsgCode', backref='application', lazy='dynamic')

    def __repr__(self):
        return '<Application {}>'.format(self.name)


class MsgCode(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(128), index=True, unique=True)
    description = db.Column(db.Text())
    resolution = db.Column(db.Text())

    application_id = db.Column(db.Integer, db.ForeignKey('application.id'))

    def __repr__(self):
        return '<MsgCode {}>'.format(self.code)
