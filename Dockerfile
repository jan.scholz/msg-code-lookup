FROM python:3.6-alpine
RUN apk --update add \
  gcc \
  libffi-dev \
  libressl-dev \
  musl-dev \
  postgresql-dev \
  python3-dev \
  && rm -rf /var/cache/apk/*
ENV APP_ROOT=/code
ENV PATH=${APP_ROOT}/bin:${PATH} HOME=${APP_ROOT}
ENV FLASK_APP=msg_code_lookup
WORKDIR ${APP_ROOT}
ADD requirements.txt ${APP_ROOT}
RUN pip install -r requirements.txt
ADD . ${APP_ROOT}
RUN chmod -R u+x ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd

EXPOSE 5000

### user name recognition at runtime w/ an arbitrary uid - for OpenShift deployments
#ENTRYPOINT [ "uid_entrypoint" ]
#ENTRYPOINT [ "run.sh" ]

#CMD ["flask", "run"]
#CMD ["run.sh"]
