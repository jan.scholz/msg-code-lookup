"""
You can auto-discover and run all tests with this command:

    $ pytest

Documentation:

* https://docs.pytest.org/en/latest/
* https://docs.pytest.org/en/latest/fixture.html
* http://flask.pocoo.org/docs/latest/testing/
"""

import pytest


@pytest.fixture
def app():
    from app import app
    app.debug = True

    from app import db
    from app.models import Application, MsgCode
    m1 = MsgCode(code="TEST456TEST")
    m2 = MsgCode(code="TEST789TEST")
    db.session.add(Application(name="AnAppName", msgcodes=[m1, m2]))

    return app.test_client()


def test_index(app):
    res = app.get("/")
    # print(dir(res), res.status_code)
    assert res.status_code == 200
    assert b"Useful hints instead of cryptic error codes" in res.data
    assert b"AnAppName" in res.data


def test_some_id(app):
    res = app.get("/foo/12345")
    assert res.status_code == 200
    assert b"12345" in res.data


def test_app_page(app):
    res = app.get("/AnAppName/")
    assert res.status_code == 200
    assert b"AnAppName" in res.data
    assert b"TEST456TEST" in res.data
    assert b"TEST789TEST" in res.data


def test_unknown_application(app):
    res = app.get("/UnknownApp/")
    assert res.status_code == 404


def test_code_page(app):
    res = app.get("/AnAppName/TEST456TEST")
    assert res.status_code == 200
    assert b"TEST456TEST" in res.data
    assert b"AnAppName" in res.data


def test_unknown_code(app):
    res = app.get("/AnAppName/UnknownCode666")
    assert res.status_code == 404
