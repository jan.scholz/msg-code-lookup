Message Code Lookup
===================

> Useful hints instead of cryptic error codes

`msg-code-lookup` is a simple web application that lets developers provide
user-comprehendible hints for otherwise cryptic error messages of their
software.

While we're all aware that ideally an error message is precise,
comprehendible, and concise anyway, using an external server to manage and
improve the error messages after the software has been released.

Extended explanations of the error message, reasons for the occurrence of the
error, and hints how the user may solve the issue will be displayed at
[http://servername?code=ERRORCODE], documented error codes for an
application are available at [http://servername/APPNAME].
